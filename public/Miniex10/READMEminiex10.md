The flowchart in this mini exercise is based on miniex4: https://mikkellundager.gitlab.io/ap2020/Miniex4/

Repository for miniex4: https://gitlab.com/MikkelLundager/ap2020/-/tree/master/public%2FMiniex4

The following two flowcharts were made together with the other members of group 7 (https://gitlab.com/MagnusBak, https://gitlab.com/JaneCl, https://gitlab.com/julie_steffensen).

For miniex4 i created a flowchart that is based on my understanding of the program; it should seemingly illustrate how i perceive the computer to execute the program and
how the "thought-process" that lies behind this execution can be mapped and explained in programming terms. The way in which i chose what to represent in the flowchart is 
based on how the different functions and commands are "grouped" in the source code and my understanding of how the program executes both the setup - and draw-functions and 
how they differentiate from each other. Starting from the top and scrolling down the program executes the top lines of codes first and this is reflected in the way i created the
flowchart. 

![Screenshot](screenshot_3.PNG)

The first flowchart is based on an idea that deals with the themes of games, data capture and privacy - the program displays an iPhone that interacts with the user via texts and
allows you to answer. Once a predetermined cycle of exchanges of texts have occurred the phone changes to a state of virus-like "blackout" that lets the player know that the
"private" texts have been intercepted and the phone is taken over. The game ends with the screen of the phone turning black.

![Screenshot](screenshot_2.PNG)

The second idea sees the player designing his or hers own humanlike avatar. The game draws inspiration from real life stats-cards like the ones that can be found in a variety
of card - and online games. The game lets you choose between different starting characters that become yours to customize - the characters each have their own starting stats and
these can be changed to the players liking. Once the customization is completed the player will recieve a complete "evaluation" of the character based on predetermined variables.
From here the user can either choose to go back and change the current stats or pick another character and start over.

![Screenshot](screenshot_1.PNG)

<b> What are the challenges between simplicity at the level of communication and complexity at the level of algorithmic procedure? </b>

The complexity of algorithmic procedures can be quite difficult to properly display in a flowchart - as they are made with simplicity in mind - and these procedures can take on
vastly different characteristics once initialized and executed in a program rather than just being imagined in a flowchart as part of a routine. It seems that the difficulty lies
in not making the flowchart obsolete once the programming starts, as it quickly becomes "easier" to just code a program to your immediate liking instead of having to rely on
something that was created prior to even beginning with the code.

<b> What are the technical challenges for the two ideas and how are you going to address them? </b>

We have discussed that the possible challenges that lie ahead of whichever idea we chose to go forward with may not lie in the execution of the code itself - as we feel confident
that these ideas are somehow possible for us to make, as this was a central concern to us when we initially started brainstorming and the ideas took shape. We agreed that a good
starting point would be other peoples work, namely the projects that people have created in the p5.js-editor that are available online, for inspiration and help with the source
code. Researching different ideas of other people was what initially gave us these ideas and helped us develop a scope of what exatcly is possible to do with p5.js in regards
to our specific idea.

<b> What is the value of the individual and the group flowchart that you have produced? </b>

In addition to being an exercise in creating programming flowcharts for the first time in Axure, and getting familiar with the structures and paths of the program, we all agreed
that this created some artistic value for us in order to externalize our ideas and somehow quickly concretize a given idea in a medium that is both approachable and customizable
for us, internally in group 7, but also for others when we seek to make our ideas apparent and comprehensive. 
