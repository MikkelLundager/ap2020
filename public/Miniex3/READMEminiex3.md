![Screenshot](screenshot.png)

RUNME: https://mikkellundager.gitlab.io/ap2020/Miniex3/

Repository: https://gitlab.com/MikkelLundager/ap2020/-/tree/master/public/Miniex3


I have created a cursor displaying the word "Loading" that can change in some different ways; if the cursor is moved to the right it changes from "Loading" to "Ready" and
if the mouse is clicked it changes the rotation of the text. The text rotates around the Z-axis and the color of the text is generated randomly from the red-spectrum with a little
bit of green as well and no blue. 


For this exercise i took to using variables in order to make the progress of animation more simple for myself. I loaded WEBGL in order to animate a rotation for my text and it
required me to define the text by loading a font, so i used loadFont(); and function preload(){} in order to retrieve the Times New Roman font for my text and create a rotation.
I used rotateZ for the rotation and created the variables let = angle = 0; for the rotation and let = angleNum = 3; for the speed of the rotation. AngleMode(DEGREES) lets p5 calculate in
angles and this i needed for the rotation. Translate was useful in order to get the text to the mouse coordinates for the cursor, and also to restore the origin drawing point when
using WEBGL. I used an if-else statement to get the text to change according to the x-coordinate of the mouse position and function mousePressed(){} to reverse the direction of the
rotation.

What relates to time here is the way in which the text changes from "Loading" to "Ready", as this is something an ordinary throbber would never do; an ordinary throbber does not 
usually inform you about anything in the progress as you, as the user awaiting further action, never get a change to acknowledge or inspect the intricate processes behind the
throbber. This could ultimately lead to you feeling like your time has been wasted; when "Ready" appears on the screen, and you now know that the program has "finally" finished
loading, you regain a sense of this lost time and may feel that your time waiting was not wasted after all. 
