//load a font for WEBGL
let myFont;
function preload(){
myFont = loadFont('Fonts/times.ttf');
}
//create a variable to use for rotation
let angle = 0;
let angleNum = 3;
function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL)
//set the angleMode to degrees for rotation
  angleMode(DEGREES)
}
function draw() {
//move the origin point of drawing to top left corner again, because WEBGL centers drawing
translate(-width/2,-height/2,0);
background(85)
//choose font, size and color of text
textFont(myFont)
fill(random(255), 204, 0)
textSize(50);
//move text to same coordinates as mouse position and rotate around Z axis
translate(mouseX, mouseY)
rotateZ(angle);
//place at the center of the cursor
textAlign(CENTER)
//determine how fast the text will be rotating in each direction
angle = angle + angleNum;
//move the cursor to the right (higher than x=500) and the text changes
if(mouseX >= 500){
text("Ready", 0, 0)
} else {
text("Loading", 0, 0)
}
}
//change direction of text rotation when the mouse is pressed
function mousePressed() {
angleNum = angleNum*-1;
}
