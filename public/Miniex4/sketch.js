//create variables to use with button
let buttonWidth = 35
let buttonHeight = 35
let buttonX = 250
let buttonY = 290
//create arrays with positions for both the X and Y-axis
let xPos = [];
let yPos = [];
function setup() {
//slow framerate in order to see colors better
frameRate (35);
//set the positions along both axis to be 35 pixels apart
for (let i = 0; i < 500/35; i++){
xPos [i]= i*35
yPos [i] = i*35
  }
createCanvas(500, 500)
}
function draw() {
noStroke()
background(0)
//create a grid
//Use a loop to create rows on X and Y-axis that have the same variables as the arrays
for (let x = 0; x < xPos.length; x++){
for (let y = 0; y < yPos.length; y++){
//generate random color from green spectrum accross the rows
fill(0, random(255), 0)
//create ellipses on each of the Y and X-positions
ellipse (xPos[x], yPos[y], 15)
  }
}
//create text "PRESS THE BUTTON" in the middle
fill('lightgreen')
textSize(30)
textFont('Courier')
textAlign(CENTER)
text('PRESS THE BUTTON', 250, 250)
//create an ellipse below the text to use as button by using the button-variables
strokeWeight(3)
stroke('red')
fill(127, 235, 127)
ellipse (buttonX, buttonY, buttonWidth, buttonHeight)
}
//When you click inside the button it moves to a random location on the canvas
function mousePressed(){
//check if mouse is pressed inside the button
let d = dist(mouseX, mouseY, buttonX, buttonY)
if (d < buttonWidth/2){
//if mouse is pressed inside the button generate random coordinates for new location inside canvas
  buttonX = random(buttonWidth/2 ,width-buttonWidth/2);
  buttonY = random(buttonHeight/2 ,height-buttonHeight/2);
  }
}
