![Screenshot](screenshot.png)

Repository: https://gitlab.com/MikkelLundager/ap2020/-/tree/master/public%2FMiniex4

RUNME: https://mikkellundager.gitlab.io/ap2020/Miniex4/

I have chosen to name my work "The Chasing Button":

As a grid of blinking green ellipses are flashing in the background, a button appears in the middle underneath some text reading: "PRESS THE BUTTON".
Everytime the mouse is pressed while the cursor is inside the button it changes its location, encouraging the user to "chase" the button around the canvas, perhaps thinking that
something will happen eventually, but nothing ever happens with the pressing of the button. 

For this exercise i encouraged myself to start thinking differently about the way i use variables and implement some new commands in my program. I figured out how an array can be
used together with the for loop, in order to make the grid of ellipses, and this undoubtedly helped me both keep the amount of source code at a minimum and create something with
more elements. The array was used to store variables that determine some points on both the X and Y-axis for the grid of ellipses, and the for loop is used to create rows of
ellipses on each of the points. Instead of using the DOM-button i used another ellipse, with a different layout to distinguish it from the rest, as this seemed easier to implement
in the existing code as i was planning on using function mousePressed() for the data capture-part and knew how this would work with a simple shape such as the ellipse. 
The function mousePressed() is used both to determine whether the mouse is pressed inside the button or not and to relocate it to a random coordinate on the canvas if it is
pressed inside the button. I made sure to create my button using variables that made sense to me for further use and in order to make it easier for myself to understand the
source code and retrace my steps at a later point. 

The grid in the background is supposed to represent the vast web itself, as it is moving and flashing at a high rate, to symbolize what goes on in the "background" as you are
clicking buttons and searching for/viewing content online. The buttons behavior is a reflection of how online behavior have changed over the course of this digital, new age
where every humang being in possesion of a smart device leaves an online trail everywhere they go; in the search of new buttons, new content and new entertainment the aspect of
personal and online safety and basic rights is often cast aside in order to smoothen out - and minimize the amount of time waiting. In a culture where everything is designed and
introduced to be on-demand and available everywhere to everyone "The Chasing Button" leaves you waiting for something that never happens; while you're stuck in the flashing grid
of the web, searching the next thing to entertain you, the escaping button that can never be "caught" encourages you to stop and think about what implications and repercussions
are connected to your online behavior and what this implies in terms of cultural and social norms regarding data capture and online presence in a postmodern and digital age of
technological luxury. 

