let c;
function setup() {
  createCanvas(450, 500, WEBGL);
  requestPointerLock();
  cam = createCamera();
  c = color(255, 204, 0);
}
function draw() {
fill(c);
stroke('red');
strokeWeight(4);
  background('#222222');
  cam.pan(-movedX * 0.001);
  cam.tilt(movedY * 0.001);
  rotateX(frameCount * 0.01);
  rotateZ(frameCount * 0.01);
    arc(50, 50, 80, 80, 0, PI + QUARTER_PI, PIE);
}
function mousePressed() {
    if (c === 'red') {
     c = color(255, 204, 0);
  } else {
    c = 'red';
  }
}
