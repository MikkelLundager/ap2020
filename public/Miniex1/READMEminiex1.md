![ScreenShot](Udklip.PNG)

 https://mikkellundager.gitlab.io/ap2020/Miniex1/

• How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?
- Very frustrating and exciting
• How is the coding process different from, or similar to, reading and writing text?
- language can be understood despite a few mistakes, but coding seems to require a lot more precision
• What does code and programming mean to you, and how do the assigned readings help you to reflect on programming?
- I meant that i got a good understanding of different aspects of coding before starting to learn