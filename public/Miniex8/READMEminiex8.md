This program is made by Mikkel Lundager and Magnus Bak Nielsen of group 7.

![Screenshot](screenshot.png)

RUNME: https://mikkellundager.gitlab.io/ap2020/Miniex8/

Repository: https://gitlab.com/MikkelLundager/ap2020/-/tree/master/public/Miniex8


Our work is called <b>"House of Gods"</b>, as a reference to Mount Olympus from greek mythology, as this draws on what was central to us in coming up with the idea for - and creating this
program; the mythological creatures and their different features really inspired us, and when we discovered the existence of poetry about these fantasy-creatures we saw an opportunity
to combine the two into this. The program groups different creatures by habitat and serves bits and pieces of different poems, mashed together and consisting of different parts
from different sources, along with each creature-name; as if the creatures themselves are "speaking up" and improvising poetry in understandable, human language. 


All the headers, colors and features of the text, the names, habitats and poems are written and configured with HTML in a separate JSON-file that is accessed in the program
and listed categorically. The program runs two for loops that each show the headings and the poem-text that is attributed to each creature - this is run once in function setup()
and function draw() is removed entirely from the program, as we did not feel it was necessary to include when the program does not feature animation or different states of 
execution. The picture of Mount Olympus that is loaded was supposed to act as a background for the entire canvas, but the JSON-data is listed beneath it (we couldn't figure out why?)
and this also works to some degree in displaying our work, so we let it stay as is. 


In their text "The Aesthetics of Generative Code" Geoff Cox, Alex McLean, and Adrian Ward writes in the very beginning of the paragraph concerning Poetics:

<i>"Like poetry, the aesthetic value of code lies in its execution, not simply its written form. To appreciate it fully we need to ‘see’ the code to fully 
grasp what it is we are experiencing and to build an understanding of the code’s actions"</i>

This stands to be very much the case of our shared experience with this mini-exercise; underway as this program took form and we thought off all kinds of different ideas,
some too advanced for our programming skills and others to abstract, what gave us the final idea was the idea of how the code, and especially the data from the JSON-file, would
need to be implemented and initialized first in a basic program that could then eventually be modified and evolved, in order for us to fully grasp what was possible and how
we could make it our own. We initially "borrowed" pieces of Daniel Shiffmann's code from his instructional videos on JSON, in order for us to firstly get the JSON working and
then to move on to how we could utilize its data-storing properties in our own work. 

In addition to this Geoff Cox and Alex McLean have also written the following in their book "Speaking Code: Coding as Aesthetic and Political Expression", in chapter 1,
called "Vocable Code", on page 24. This relates to the programming language Perl and its creator Larry Wall:

<i>"Wall was emphasizing the point that code has expressive qualities, and the need for programmers to "express both their emotional and technical natures simultaneously.""</i>

For this program we tried to equally distribute our focus on what was computationally possible with the code and what was linguistically possible with the poetry and all the 
different fantasy-words derived from the worlds of mythological beasts; the idea of the creatures themselves becoming lyrical and poetic presented itself to us as the best 
possible middle ground for this to be achieved. It allowed us to create a program with simple, functional syntax and put extensive effort into the JSON-file and its content
in order to research the creatures and the poetry related to them. This divide in attention was unknowingly our attempt to express both an emotional state, a fascination with
mythological beasts and a technical nature that could allow us to create something with our programming skills - however limited these might be. 
