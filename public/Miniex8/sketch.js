var data;
var img;

function preload(){
  //load JSON-file with mythical creatures to display in program
  data = loadJSON('creatures.json');
  //load image for background
  img = loadImage('Image/mount_olympus.jpg')
}

function setup(){
  //make canvas same size as img and use img as background
  createCanvas(726, 1075)

  background (img);
//get data from array in JSON-file
var creatures = data.creatures;
//create headers with each habitats of the creatures
for (var i = 0; i < creatures.length; i++) {
  createElement('h1', creatures[i].habitat);
//establish type of creature and create divisions of types for the creatures
  var type = creatures[i].type;
for (var k = 0; k < type.length; k++) {
  createDiv(type[k]);
    }
  }
}
