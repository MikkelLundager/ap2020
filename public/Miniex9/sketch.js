//MiniEx 9 - working with API's
//Group 7 - Julie, Mikkel, Magnus and Jane
//API data from NASA

var url = 'https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=b24ilsUblK3KodeGafz7BaHAeuc42PI01eBODdI5'
let rocket;
//let track;
let roverPicture;
let i

function preload(){
//  track = createAudio('Assets/Startrek_theme.mp3')//
  rocket = loadImage("Assets/rocket.png");

}

function setup() {

  createCanvas(windowWidth,windowHeight);
  noCursor();


//  track.autoplay(true);
}

function gotData(data) {

    roverPicture = data.photos[i].img_src
    let marsPicture = createImg(roverPicture,"");
  marsPicture.position(windowWidth/2-200,windowHeight/2-250);
  marsPicture.size(900,700);

}

function draw() {

  background(50);
  //the rocket cursor
  image(rocket, mouseX, mouseY, 60, 60);
push();
  fill(200);
  textSize(45);
  text("Click mouse for new picture",windowWidth/2, 45);
  textAlign(CENTER);
pop();
}

//This is where a function Mousepressed should allow you to change between photos with a Click
function mousePressed() {

  i = floor(random(0, 50));
loadJSON(url, gotData);
  console.log('pressed')
  console.log(i)
}
