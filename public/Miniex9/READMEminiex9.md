![Screenshot](screenshot.png)

RUNME: https://mikkellundager.gitlab.io/ap2020/Miniex9/

Repository: https://gitlab.com/MikkelLundager/ap2020/-/tree/master/public/Miniex9

<b>Who are you collaborate with / which group?</b>

We are group 7 - Magnus Bak (https://gitlab.com/MagnusBak), Julie Steffensen (https://gitlab.com/julie_steffensen), Jane Clausen (https://gitlab.com/JaneCl) & Mikkel Holger

<b>A title of your program</b>

Ticket to mars

<b>OBS: When running the program click once and wait for the picture to load before clicking again.</b> 

<b>What is the program about? which API have you used and why?</b>

Our program uses the API from Nasa. Nasa is a U.S government agency that is responsible for science and technology related  to air and space. They receive 16 billion dollars of funding each year from the american state for space research. This makes NASA the largest space agency in the world. In addition to that NASA helps teachers by sharing updated research material to educate better engineers, scientist and astronauts in the future. 

Much of their information are presented as open API files on their website. creating an API key was easy and did not  require much information from the user. After receiving the API key we could choose from a various of API’s with bot pictures and information from NASA recent research. 

Space is an wonderous black mass  full of wonders and enigmas. For many space brings many question and curiosity. It is a mystery and it holds the key to our origin and as the case may be our future. That is why we chose NASA as the source of our API. In addition to that we want to support this way of creating open source data for all of  public to view and use. 

We chose the API that holds pictures of mars taken every day since 2015. This API key is called Mars Rover Photos. This API is designed to collect image data gathered by NASA's Curiosity, Opportunity, and Spirit rovers on Mars and make it more easily available to other developers, educators, and citizen scientists. The API provides a daily picture of mars taken by the MARS ROVER. 

The concept for our program is a “Rocket to Mars”. we wanted in the most humble way to create an experience of flying to mars. The mouse cursor is a rocket flying in space wherever the user want it to. When clicked the mouse the picture of mars changes in a random order. When mouse is first clicked we wanted to make the audio from star trek play in the background to add to the experience of flying through space. 

Sadly after countless hours of trying we had trouble making the sound work but we have uploaded the audio separately and has kept the code cursed out in our sketch file. 

<b>Can you describe and reflect your process of making this mini exercise in terms of acquiring, processing, using and representing data? How much do you understand this provided data? How do platform providers sort the data and give you the selected data?</b> 

We spent a significant amount of time researching API’s and determining which one to use. we went through a lot of different platforms searching through the vararing way of setups provided by the different API’s. We went through a lot of API’s that was supported by node.js and not P5.js. We made a decision in the group to look for a P5.js friendly API to avoid  making the assignment too complicated. We found the NASA website that provided API data in a JSON format. This was familiar and easy to use. We used the text  pasing formatter to create a better overview of the data and in that way making them easier to work with. 

<b>What are the power-relationship in the chosen APIs?</b> 

Even though NASA has a lot of information open source for the public to  view and use. They are the ones who  chose what the public gets to see and  what they do not get to see. They are an American based agency which makes them impartial and english based. Their research takes shape in America and revolves around America. 

<b>What's the significance of APIs in digital culture? (If possible, please make an explicit linkage with the assigned reading)</b>

According to the text API have had a significant impact on socio technical culture. However we chose to implement an API in a different way. We used the API as a way of accesing and using information upon a from a research field.  
“[...] APIs set upon practices of knowing, sharing, participation and exchange.”
We wanted to create a tool more suitable for teaching and learning our program is not social but a way of keeping updated on the newest information on the field of  space images. 
“At a basic level, APIs do the work of exposing and making available the resources of particular programmatic components so that they can be accessed by other programmatic components. “
Our program use the resources of NASA’s research to support our program. We are using their images to browse through the ever changing surfaces of Mars. We have taken some relatively complex data from a  professional space agency and implemented it in our program which is quite remarkable. The ability to parse data on through varying layers of skill. 
In digital culture today users are accustomed to browsing and moving through websites without boundaries or any stops. When we get an email with an invitation to an event the appointment appear in the users calendar. This is just one example of the massive use of API’s throughout the web.  
Although this makes the experience easier and  more fluent for the user. The  information being shared through the platforms may sometimes end up in third party hands without the awareness of the user. 

<b>Try to formulate a question in relation to web APIs or querying processes that you want to investigate further when you have more time.</b>

In relation to the find that a big corporation like NASA have some of  their information available to the public. It would be interesting to explore if there are other big corporations that have some of their data open source like NASA. 
