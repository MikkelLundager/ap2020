//create array for class
let stars = [];
//create image to use for background
let img;
//create track to play when program opens
let track;
function preload(){
  img = loadImage('Images/deep_space.jpg');
}
function setup() {
  frameRate(80);
  //find track and play when possible
  track = createAudio('Music/deep_space_music.mp3');
  track.autoplay(true);
  //create canvas that have same dimensions as img
  createCanvas(696, 392);
  //determine number of objects (200) in array and make position, size and shape random
   for (let i = 0; i < 200; i++){
     let x = random(width);
     let y = random(height);
     let d = random(0, 12);
     let r = random(0, 2);
     let shape = floor(random(3));
  //create new object from class
    stars[i] = new Stars(x, y, d, r)
   }
}
function draw() {
  //use image as background
  background(img);
  //show and move objects from array as long as index is lower than the amount in array
  for (let i = 0; i < stars.length; i++){
    stars[i].move();
    stars[i].show();
  //check if mouse is placed on top of each individual star and remove it from array if true
    if (dist (mouseX, mouseY, stars[i].x, stars[i].y) < stars[i].d + 8) {
    stars.splice(i, 1);
    }
  }
}
//create class for stars with x, y, diameter, radius and shape-variables
class Stars {
constructor(x, y, d, r) {
  this.x = x;
  this.y = y;
  this.d = d;
  this.r = r;
  this.shape;
}
//create a shivering motion on each star
move() {
this.x = this.x + random(-3, 3);
this.y = this.y + random(-3, 3);
}
//randomize colors in stars to make them "glow" in different colors
show() {
fill(random(235));
stroke(random(235), random(235), random(235), 75);
strokeWeight(10);
//choose between circle, triangle and rectangle as shape depending on what random number is generated
if (this.shape == 0) {
circle(this.x, this.y, this.d);
} else if (this.shape == 1){
triangle(this.x, this.y, this.x-3, this.y +5, this.x+3, this.y+5);
} else {
rect(this.x, this.y, this.d, this.d, this.r);
        }
    }
}
