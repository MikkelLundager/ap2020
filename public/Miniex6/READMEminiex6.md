![Screenshot](screenshot.png)

RUNME: https://mikkellundager.gitlab.io/ap2020/Miniex6/

Repository: https://gitlab.com/MikkelLundager/ap2020/-/tree/master/public%2FMiniex6

I have made a "starry night in space"-kinda thing and it shows a preloaded picture of space as the background, along with some stars appearing as different shapes and
"glowing" in different colors and some ambient space music with an atmospheric vibe and different space-mission calls playing simultaneously. As you move the mouse around
and it appears on top of each individual star it "erases" it, allowing you to clean the canvas by just moving the cursor around.

I have created a class for the stars and assigned different variables (x, y, diameter, radius and shape) to it, in order to make them appear as multiple objects with different,
randomized characteristics, to simulate how the forces of nature works differently around the universe and how this influences what you can observe with the naked eye - even
though a closer look would reveal the many dissimilar features of what we as one big body of objects define as "stars". They do not interact with each other, but the mouse; as
the program runs it continually checks the mouse-position all over the canvas, and if the mouse is placed on top of, or slightly to the side of the center of a star it will be
removed from the canvas and leave just the "empty" space-background and ambient music still playing. 

To me it seems that object-oriented programming concerns both the tangible - and intangible creations of both nature and the human mind; in order to make something tangible in terms
of programming it must be objectivized and defined as an entity to further work with and alter in any way. In nature things are as they are, limithing as it may seem to artists and
people alike, but in the world of programming one can abstract pretty much anything from the world of the intangible and make it tangible in terms of code. This implies abstraction
to be a subjective action that varies from each perspective to the next. The implications of abstraction, not only in terms of objects and classes but also people, seems therefore
to be that agreements on definitions can allude to what is individually subtracted from a certain topic or object and may not always coincide.
Abstractions can, on the other end of the spectrum, have unifying and collateral effects too as they allow us to determine and understand the complex and dynamic relations that
make up our everyday-life; when we use a computer, a smartphone, a tablet or something alike, that runs on electricity and acts computationally on humand demands, we partake
in a series of complex and carefully abstracted computer-mechanisms that merge our cultural understanding of what we are doing and the computers understanding of what we want
it to do; the different implications and intetions of abstraction work all around us everyday as we use technology and each other in ways that sometimes require us to extrapolate
and imagine things beyond what is given to us by nature and appear visually and/or physically available.