//variable to use with mousePressed
let fillColor
//variable to use in if-else statement
let backgroundColor
//variable to use to return conditions with mousePressed
let reverse = false
function setup() {
  createCanvas(500, 500)
  fillColor = color(255, 204, 0)
  backgroundColor = color(0, 0, 0)
}
function draw() {
//create arc with closed PIE top and set background using variable
fill(fillColor)
stroke(255, 0, 0)
strokeWeight(4)
background(backgroundColor)
//get arc to follow mouse coordinates
arc(mouseX, mouseY, 80, 80, 0, PI + QUARTER_PI, PIE)
//change background color depending on cursor position
if (mouseX >= 255){
backgroundColor = color(0, 255, 0)
}
else if (mouseY >= 255){
backgroundColor = color(0, 0, 255)
}
else {
backgroundColor = color(255, 0, 0)
  }
}
//change between two colors with pressing of the mouse
function mousePressed() {
//return true for false and false for true to reverse effects of color change
  reverse = !reverse
// if false then one color for fill
if (reverse == false){
  fillColor = color(255, 0, 0)
}
// if true then another color for fill
if (reverse == true){
  fillColor = color(255, 204, 0)
}
}
