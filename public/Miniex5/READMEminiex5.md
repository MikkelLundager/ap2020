![Screenshot](screenshot.png)

RUNME: https://mikkellundager.gitlab.io/ap2020/Miniex5/

Repository: https://gitlab.com/MikkelLundager/ap2020/-/tree/master/public%2FMiniex5

For this weeks exercise i have chosen to rework my Miniex1 - upon revisiting my work i realised that it was filled with syntax i did not quite know and i decided to trim it down.
It makes no educational or progressional sense to me to just use syntax without intent or meaning behind it, so i removed everything that seemed "out of place" with my current
knowlegde and included new commands that i knew free of the reference-page at p5.org. 
I hope for this be an expression of artistic choice that enables everyone creating something in one manner of the other to be able to just stop and think about the progression and
choices underway; this is not the type of work to be displayed for the sake of impressing or creating something entirely new and unseen, maybe not even for others to ever see,
but more for myself to know that i can get behind what is created and have not just taken something of the internet without intent to modify it or otherwise create something new
with it.
I hope to express a freedom of choice for creation that bases intuition on feelings and intent rather than pretentious search for acknowlegdement. To me the difference between a
good and a bad program lies not in the source code alone, but also in the intent of creation and thought process poured into the work as well. 
I have not changed as much as i have removed things in this program, although a few new commands were added. I created some variables that were used to change both the fill color
of the arc with function mousePressed() and the backgroundcolor with if-else statements that control the color based on the position of the cusor. Nothing entirely original was
created with this, nor was any never-seen-before-syntax discovered and utilized, but i believe that to be due to my intentions being different and focus lying elsewhere.
This freedom of choice i chose to exercise and advocate with this exercise is also reminiscent of what aesthetic programming means and embodies to me; not long ago i had no idea
what programming and code even was, could do or looked like and now, upon realizing how many aspects of life it touches and how many fields of knowlegde it encompasses, i think
what makes it truly uniquie is the freedom of choice and uniqueness of its character and position in life, based on who you are and how many hours you pour into it. 
From the get-go i envisioned programming as a highly specialized skill that could not serve just anyone in the daily life and working environment. But it quickly dawned on me
that it can just as easily be exatcly what you want it to be, because it mostly just is fun and empowering, and therefor does not need to fullfill certain procreational requirements
or have a distinct function; it can be used to envision small ideas and maximize creativity as well - and that is what i believe to be the most prominent and diverse feature of
aesthetic programming.