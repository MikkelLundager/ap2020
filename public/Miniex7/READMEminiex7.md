![Screenshot](screenshot.png)

RUNME: https://mikkellundager.gitlab.io/ap2020/Miniex7/

Repository: https://gitlab.com/MikkelLundager/ap2020/-/tree/master/public%2FMiniex7

For this mini-exercise i have created what i consider to be a "wallpaper"-type of program; my inital intention with this program was to create an animated background/wallpaper, that 
would both have static and animated elements, which could be aesthetically pleasing but not have interaction and/or complicated syntax/functions.

This program draws on the PRINT 10-program and adds new functions and layout to something already in existence. I changed the generation of rows from the x to the y-axis, so that 
they are created vertically, instead of horisontally as in the original program. The static background, with the lines of different colors emerging from the corner, is created
using a for loop that i placed in setup in order for it to run only once and remain static. Instead of only lines of opposite directions the program now also creates both circles
and rectangles at random as it runs, in order to create a randomized pattern of shapes as well as with the lines.

One rule that is persistent with my program is that all motion and animation happens vertically - when i saw the original PRINT 10 i immediatly thought that the motion would look 
more interesting vertically rather than horisontally - the change in syntax to alter this direction of animation was minimal and it turned out to fit better with my initial idea
of creating a static background and have the program do something generative with shapes in the foreground. 
The rules i established before initializing this program allow for easy conception of motion and a high degree of predictability; when the program runs it does not require interaction
and it does not change neither direction nor layout throughout, and this i believe lends itself to create better possibilities for transparency and fluency. There are no political or
social values imbued in this program, as it serves as a sensual, temporal exploration of motion and patterns (because although the patterns are randomized they still appear fixed and tidy).

In addition to the patterns, and how they visually order themselves throughout the random generation, the colors also seems to align in spite of being varied and different;
the concept of the pseudo-randomness comes into play here, as the computer is limited by the limits of my program in terms of "true" randomness: In order to truly randomize the
selection of both the shapes, lines and colors i believe the program would appear very differently, if this true randomness was the case, as it would perhaps render completely
unorganized and without patterns and aesthetically pleasing motives to captivate and engage. The pre-determined factors that come into play as the computer randomized choices and
values in a program seem to at large create some sort of order in what is supposed to be out of order, as values, shapes and colors vary between each frame and rendition. 