//variables to use with lines and shapes
let x = 0;
let y = 0;
//distance on x-axis between vertical lines
let distance = 25;

function setup() {
  createCanvas(800, 800);
  background(0);

//create a static background-image with lines emerging from top right corner
  for (let i = 0; i < 150; i++){
    let x = 800;
    let y = 0;
    stroke(random(100), random(200), random(255))
    line(x, y, random(windowWidth), random(windowHeight))
  }
}

function draw() {

//generate random colors both on lines (stroke) and on shapes (fill)
  stroke(random(100), random(200), random(255), 180);
  fill(random(100), random(200), random(255), 180);

//if a random number between 0 and 1 is less than 0.25 create line in one direction
  if (random(1) < 0.25) {
    line( x, y, x + distance, y + distance);
//if less than 0.50 create line in opposite direction
  } else if (random(1) <0.50) {
    line(x, y + distance, x + distance,y);
//if less than 0.75 create a rectangle
  } else if (random(1) <0.75){
    rect (x, y, 10, 10);
//if above 0.75 create a circle
  } else {
    circle (x, y, 10, 10);
  }
//y = y + 10
  y += 10;
//start new vertical row along y-axis when it reaches the bottom of canvas
  if (y > height) {
    y = 0;
//x = x + distance (25 pixels)
    x += distance;
    }
  }
