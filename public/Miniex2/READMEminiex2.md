![Screenshot](screenshot.PNG)

Link for RUNME https://mikkellundager.gitlab.io/ap2020/Miniex2/

Link for repository https://gitlab.com/MikkelLundager/ap2020/-/tree/master/public/Miniex2

For this sketch i have drawn two Emojis using contrasts; rather than using "just" a black stroke to create something on a white canvas, the background is painted black in order for
the white shapes to "break through" and create the emoji. Rather than creating an entirely new emoji i used existing traits from ones i know, but tried to envision them on a new
canvas and showcase them in a more cartoonish and neutral way that does not seek to duplicate the real world, and thereby avoid the problems that arise if i were to attempt to make
them realistic. The problem outlined in the text "Modifying the Universal" was taken into account during the making of this, as the problem with imposing such a thing as the Unicode
Standard can unknowingly have repercussions regarding the normalization and opression of minorities; the way in which these emojis are made is an attempt to bypass such problems and
render the faces neutral and approachable by everyone. 

In creating theese two i mainly used the "simple" and basic syntax: Shapes such as circles, rectangles, triangles, lines and curves where used, and occasionally it was required to
use other tools (a radians for example) in order to either turn or otherwise position the shapes correctly in relation to each other. Different fills were used for each emoji in
order to distinguish them from each other. It may not be fairly complicated, but i found it to be suiting for an exercise of this kind. 