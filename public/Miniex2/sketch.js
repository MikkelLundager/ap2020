function setup() {
//size of canvas
  createCanvas(600, 600)
}
function draw() {
//drawing of first emoji
//black background
noStroke()
background(0)
//rectangles used to make eyes
fill(255)
rect(100, 50, 50, 50)
rect(200, 50, 50, 50)
//circles are used to make pupils
fill("red")
circle(125, 75, 15)
circle(225, 75, 15)
//triangle to make a nose
fill(255)
triangle(150,150,175,120,175,150)
//arc is used to make a mouth and radian to turn it
arc(165, 175, 80, 80, 0, radians(180), CHORD)
//line is used to divide mouth in half
stroke(0)
line(50,190,300,190)
//curve is used to draw eyebrows
fill(255)
curve(200, 50, 100, 25, 250, 35, 250, 150)
//drawing of second emoji
//rectangles and a line is used to draw sunglasses
fill("orange")
rect(350, 50, 75, 50, 20)
rect(450, 50, 75, 50, 20)
stroke("orange")
strokeWeight(5)
line(350, 75, 525, 75)
//ellipse is used to make a nose
ellipse(435, 135, 35)
//curve is used to make a mouth
curve(200, 400, 400, 200, 450, 200, 350, 200)
//two triangles are used to make eyebrows
fill("orange")
triangle(360, 35, 420, 38, 360, 40)
triangle(455, 40, 515, 38, 515, 35)


}
